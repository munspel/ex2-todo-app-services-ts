import "bootstrap/dist/css/bootstrap.min.css";
import Vue from 'vue'
import App from './App.vue'
import { provider } from "./services/providers";
Vue.prototype.$services  = provider;
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
